from flask_login import UserMixin
from . import db

class User(UserMixin, db.Model):
    Tipo= db.Column(db.Integer, primary_key=True) # primary keys are required by SQLAlchemy
    Nombres = db.Column(db.String(1000))
    Apellidos = db.Column(db.String(1000))
    Edad = db.Column(db.String(1000))
    CorreoElectronico = db.Column(db.String(100), unique=True)
    Contraseña = db.Column(db.String(100))